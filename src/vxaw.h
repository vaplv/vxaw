/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXAW_H
#define VXAW_H

#include <rsys/clock_time.h>
#include <rsys/rsys.h>

#include <vx/vxl.h>

#ifdef VXAW_SHARED_BUILD
  #define VXAW_API extern EXPORT_SYM
#elif defined(VXAW_STATIC_BUILD)
  #define VXAW_API extern LOCAL_SYM
#else
  #define VXAW_API extern IMPORT_SYM
#endif

/* Helper macro */
#ifndef NDEBUG
  #define VXAW(Func) ASSERT(vxaw_##Func == RES_OK)
#else
  #define VXAW(Func) vxaw_##Func
#endif

/* Forward declaration */
struct aw_obj;
struct aw_mtl;
struct logger;
struct mem_allocator;

struct vxaw;
struct vxaw_report {
  struct time load;
  struct time voxelization;
  struct time build;
};

BEGIN_DECLS

VXAW_API res_T
vxaw_create
  (struct logger* logger, /* NULL <=> default logger */
   struct mem_allocator* allocator, /* NULL <=> default allocator */
   struct aw_obj* loader_obj, /* NULL <=> Internally create the loader */
   struct aw_mtl* loader_mtl, /* NULL <=> Internally create the loader */
   struct vxl_system* vxl,
   struct vxaw** vxaw);

VXAW_API res_T
vxaw_ref_get
  (struct vxaw* aw);

VXAW_API res_T
vxaw_ref_put
  (struct vxaw* aw);

VXAW_API res_T
vxaw_get_loaders
  (struct vxaw* vxaw,
   struct aw_obj** obj, /* May be NULL */
   struct aw_mtl** mtl); /* May be NULL */

VXAW_API res_T
vxaw_load
  (struct vxaw* aw,
   const size_t definition, /* Definition along the X, Y and Z axis */
   const enum vxl_mem_location mem_location,
   const char* filename,
   struct vxaw_report* report, /* May be NULL */
   struct vxl_scene** scn);

VXAW_API res_T
vxaw_load_stream
  (struct vxaw* aw,
   const size_t definition, /* Definition along the X, Y and Z axis */
   const enum vxl_mem_location mem_location,
   FILE* stream,
   struct vxaw_report* report,
   struct vxl_scene** scn);

END_DECLS

#endif /* VXAW_H */

