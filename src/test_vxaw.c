/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxaw.h"

#include <rsys/logger.h>
#include <rsys/mem_allocator.h>

#include <vx/vxl.h>

static void
test_cbox(struct vxaw* vxaw, const enum vxl_mem_location memloc)
{
  const char* cbox_obj =
    "mtllib cbox.mtl cbox2.mtl\n"
    "v -1.01 0 0.99\n"
    "v 1 0 0.99\n"
    "v 1 0 -1.04\n"
    "v -0.99  0 -1.04\n"
    "g floor\n"
    "usemtl floor\n"
    "f -4 -3 -2 -1\n"

    "v -1.02 1.99 0.99\n"
    "v -1.02 1.99 -1.04\n"
    "v 1 1.99 -1.04\n"
    "v 1 1.99 0.99\n"
    "g ceiling\n"
    "usemtl ceiling\n"
    "f -4 -3 -2 -1\n"

    "v -0.99 0 -1.04\n"
    "v 1 0 -1.04\n"
    "v 1 1.99 -1.04\n"
    "v -1.02 1.99 -1.04\n"
    "g back\n"
    "usemtl back\n"
    "f -4 -3 -2 -1\n"

    "v 1 0 -1.04\n"
    "v 1 0 0.99\n"
    "v 1 1.99 0.99\n"
    "v 1 1.99 -1.04\n"
    "g right\n"
    "usemtl right\n"
    "f -4 -3 -2 -1\n"

    "v -1.01 0 0.99\n"
    "v -0.99 0 -1.04\n"
    "v -1.02 1.99 -1.04\n"
    "v -1.02 1.99 0.99\n"
    "g left\n"
    "usemtl left\n"
    "f -4 -3 -2 -1\n";

  const char* cbox_mtl =
    "newmtl left\n"
    "Ns 10\n"
    "Ni 1.5\n"
    "illum 2\n"
    "Ka 0.63 0.065 0.05\n"
    "Kd 0.63 0.065 0.05\n"
    "Ks 0 0 0\n"
    "Ke 0 0 0\n"

    "newmtl right\n"
    "Ns 10\n"
    "Ni 1.5\n"
    "illum 2\n"
    "Ka 0.14 0.45 0.091\n"
    "Kd 0.14 0.45 0.091\n"
    "Ks 0 0 0\n"
    "Ke 0 0 0\n"

    "newmtl floor\n"
    "Ns 10\n"
    "Ni 1\n"
    "illum 2\n"
    "Ka 0.725 0.71 0.68\n"
    "Kd 0.725 0.71 0.68\n"
    "Ks 0 0 0\n"
    "Ke 0 0 0\n"

    "newmtl ceiling\n"
    "Ns 10\n"
    "Ni 1\n"
    "illum 2\n"
    "Ka 0.725 0.71 0.68\n"
    "Kd 0.725 0.71 0.68\n"
    "Ks 0 0 0\n"
    "Ke 0 0 0\n";

  const char* cbox2_mtl =
    "newmtl back\n"
    "Ns 10\n"
    "Ni 1\n"
    "illum 2\n"
    "Ka 0.725 0.71 0.68\n"
    "Kd 0.725 0.71 0.68\n"
    "Ks 0 0 0\n"
    "Ke 0 0 0\n";

  FILE* file;
  struct vxaw_report report;
  struct vxl_scene* scn;
  struct vxl_scene_desc desc;

  ASSERT(vxaw);

  file = fopen("cbox.obj", "w");

  CHK(file != NULL);
  fwrite(cbox_obj, sizeof(char), strlen(cbox_obj), file);
  fclose(file);

  file = fopen("cbox.mtl", "w");
  CHK(file != NULL);
  fwrite(cbox_mtl, sizeof(char), strlen(cbox_mtl), file);
  fclose(file);

  remove("cbox2.mtl");

  CHK(vxaw_load(NULL, 0, memloc, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(vxaw_load(vxaw, 0, memloc, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(vxaw_load(NULL, 0, memloc, "cbox.obj", NULL, NULL) == RES_BAD_ARG);
  CHK(vxaw_load(vxaw, 0, memloc, "cbox.obj", NULL, NULL) == RES_BAD_ARG);
  CHK(vxaw_load(NULL, 0, memloc, NULL, NULL, &scn) == RES_BAD_ARG);
  CHK(vxaw_load(vxaw, 0, memloc, NULL, NULL, &scn) == RES_BAD_ARG);
  CHK(vxaw_load(NULL, 0, memloc, "cbox.obj", NULL, &scn) == RES_BAD_ARG);
  CHK(vxaw_load(vxaw, 0, memloc, "cbox.obj", NULL, &scn) == RES_BAD_ARG);
  CHK(vxaw_load(NULL, 128, memloc, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(vxaw_load(vxaw, 128, memloc, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(vxaw_load(NULL, 128, memloc, "cbox.obj", NULL, NULL) == RES_BAD_ARG);
  CHK(vxaw_load(vxaw, 128, memloc, "cbox.obj", NULL, NULL) == RES_BAD_ARG);
  CHK(vxaw_load(NULL, 128, memloc, NULL, NULL, &scn) == RES_BAD_ARG);
  CHK(vxaw_load(vxaw, 128, memloc, NULL, NULL, &scn) == RES_BAD_ARG);
  CHK(vxaw_load(NULL, 128, memloc, "cbox.obj", NULL, &scn) == RES_BAD_ARG);
  CHK(vxaw_load(vxaw, 128, memloc, "cbox.obj__", NULL, &scn) == RES_IO_ERR);
  CHK(vxaw_load(vxaw, 128, memloc, "cbox.obj", NULL, &scn) == RES_OK);

  CHK(vxl_scene_ref_put(scn) == RES_OK);

  CHK(vxaw_load(vxaw, 64, memloc, "cbox.obj", &report, &scn) == RES_OK);
  CHK(report.load.sec * 1000000000l + report.load.nsec != 0);
  CHK(report.voxelization.sec * 1000000000l + report.voxelization.nsec != 0);
  CHK(report.build.sec * 1000000000l + report.build.nsec != 0);
  CHK(vxl_scene_ref_put(scn) == RES_OK);

  file = fopen("cbox2.mtl", "w");
  CHK(file != NULL);
  fwrite(cbox2_mtl, sizeof(char), strlen(cbox2_mtl), file);
  fclose(file);

  CHK(vxaw_load(vxaw, 72, memloc, "cbox.obj", NULL, &scn) == RES_OK);

  CHK(vxl_scene_get_desc(scn, &desc) == RES_OK);
  CHK(desc.lower[0] < desc.upper[0]);
  CHK(desc.lower[1] < desc.upper[1]);
  CHK(desc.lower[2] < desc.upper[2]);
  CHK(desc.voxels_count != 0);
  CHK(desc.definition != 0);
  CHK(vxl_scene_ref_put(scn) == RES_OK);

  file = fopen("cbox.obj", "r");
  CHK(file != NULL);

  CHK(vxaw_load_stream(vxaw, 128, memloc, file, &report, &scn) == RES_OK);
  CHK(report.load.sec * 1000000000l + report.load.nsec != 0);
  CHK(report.voxelization.sec * 1000000000l + report.voxelization.nsec != 0);
  CHK(report.build.sec * 1000000000l + report.build.nsec != 0);
  CHK(vxl_scene_get_desc(scn, &desc) == RES_OK);
  CHK(desc.lower[0] < desc.upper[0]);
  CHK(desc.lower[1] < desc.upper[1]);
  CHK(desc.lower[2] < desc.upper[2]);
  CHK(desc.voxels_count != 0);
  CHK(desc.definition != 0);
  CHK(vxl_scene_ref_put(scn) == RES_OK);

  fclose(file);
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator_proxy;
  struct aw_mtl* aw_mtl;
  struct aw_obj* aw_obj;
  struct vxaw* vxaw;
  struct vxl_system* vxl;
  enum vxl_mem_location memloc;

  if(argc < 2) {
    fprintf(stderr, "Usage: %s <incore|oocore>\n", argv[0]);
    return 1;
  }

  if(!strcmp(argv[1], "incore")) {
    memloc = VXL_MEM_INCORE;
  } else if(!strcmp(argv[1], "oocore")) {
    memloc = VXL_MEM_OOCORE;
  } else {
    fprintf(stderr, "Unknown memory location `%s'.\n", argv[1]);
    return 1;
  }

  mem_init_proxy_allocator(&allocator_proxy, &mem_default_allocator);

  CHK(vxl_system_create(&allocator_proxy, VXL_NTHREADS_DEFAULT, &vxl) == RES_OK);

  CHK(vxaw_create(NULL, NULL, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(vxaw_create(NULL, NULL, NULL, NULL, NULL, &vxaw) == RES_BAD_ARG);
  CHK(vxaw_create(NULL, NULL, NULL, NULL, vxl, NULL) == RES_BAD_ARG);
  CHK(vxaw_create(NULL, NULL, NULL, NULL, vxl, &vxaw) == RES_OK);

  CHK(vxaw_ref_get(NULL) == RES_BAD_ARG);
  CHK(vxaw_ref_get(vxaw) == RES_OK);
  CHK(vxaw_ref_put(NULL) == RES_BAD_ARG);
  CHK(vxaw_ref_put(vxaw) == RES_OK);
  CHK(vxaw_ref_put(vxaw) == RES_OK);

  CHK(vxaw_create(NULL, &allocator_proxy, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(vxaw_create(NULL, &allocator_proxy, NULL, NULL, NULL, &vxaw) == RES_BAD_ARG);
  CHK(vxaw_create(NULL, &allocator_proxy, NULL, NULL, vxl, NULL) == RES_BAD_ARG);
  CHK(vxaw_create(NULL, &allocator_proxy, NULL, NULL, vxl, &vxaw) == RES_OK);
  CHK(vxaw_ref_put(vxaw) == RES_OK);

  CHK(vxaw_create
    (LOGGER_DEFAULT, &allocator_proxy, NULL, NULL, vxl, &vxaw) == RES_OK);

  CHK(vxaw_get_loaders(NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(vxaw_get_loaders(vxaw, NULL, NULL) == RES_OK); /* Useless */
  CHK(vxaw_get_loaders(NULL, &aw_obj, NULL) == RES_BAD_ARG);
  CHK(vxaw_get_loaders(vxaw, &aw_obj, NULL) == RES_OK);
  CHK(vxaw_get_loaders(NULL, NULL, &aw_mtl) == RES_BAD_ARG);
  CHK(vxaw_get_loaders(vxaw, NULL, &aw_mtl) == RES_OK);
  CHK(vxaw_get_loaders(NULL, &aw_obj, &aw_mtl) == RES_BAD_ARG);
  CHK(vxaw_get_loaders(vxaw, &aw_obj, &aw_mtl) == RES_OK);

  test_cbox(vxaw, memloc);

  CHK(vxaw_ref_put(vxaw) == RES_OK);

  CHK(vxl_system_ref_put(vxl) == RES_OK);
  if(MEM_ALLOCATED_SIZE(&allocator_proxy)) {
    char dump[512];
    MEM_DUMP(&allocator_proxy, dump, sizeof(dump)/sizeof(char));
    fprintf(stderr, "%s\n", dump);
    FATAL("Memory leaks\n");
  }
  mem_shutdown_proxy_allocator(&allocator_proxy);
  CHK(mem_allocated_size() == 0);
  return 0;
}
