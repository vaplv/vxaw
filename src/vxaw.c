/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* dirname support */

#include "vxaw.h"

#include <rsys/clock_time.h>
#include <rsys/dynamic_array_char.h>
#include <rsys/dynamic_array_size_t.h>
#include <rsys/float3.h>
#include <rsys/float33.h>
#include <rsys/float4.h>
#include <rsys/hash_table.h>
#include <rsys/logger.h>
#include <rsys/mem_allocator.h>
#include <rsys/ref_count.h>

#include <vx/vxl.h>

#include <aw.h>
#include <polygon.h>

#include <libgen.h> /* dirname */
#include <unistd.h> /* access */

#define DARRAY_NAME vertex
#define DARRAY_DATA struct aw_obj_vertex
#include <rsys/dynamic_array.h>

#define DARRAY_NAME mtl
#define DARRAY_DATA struct aw_material
#define DARRAY_FUNCTOR_INIT aw_material_init
#define DARRAY_FUNCTOR_RELEASE aw_material_release
#define DARRAY_FUNCTOR_COPY aw_material_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE aw_material_copy_and_release
#include <rsys/dynamic_array.h>

#define HTABLE_NAME mtl
#define HTABLE_DATA size_t
#define HTABLE_KEY struct str
#define HTABLE_KEY_FUNCTOR_EQ str_eq
#define HTABLE_KEY_FUNCTOR_HASH str_hash
#define HTABLE_KEY_FUNCTOR_INIT str_init
#define HTABLE_KEY_FUNCTOR_COPY str_copy
#define HTABLE_KEY_FUNCTOR_COPY_AND_RELEASE str_copy_and_release
#define HTABLE_KEY_FUNCTOR_RELEASE str_release
#include <rsys/hash_table.h>

struct triangle_soup {
  struct darray_mtl mtls; /* List of materials */
  struct htable_mtl str2mtl; /* Map a material name to an index into mtls */
  struct darray_vertex vertices; /* List of triangle vertices */
  struct darray_size_t mtlids; /* Per triangle material id */
};

struct vxaw {
  ref_T ref;
  struct mem_allocator* allocator;
  struct logger* logger;
  int verbose;

  struct aw_obj* loader_obj;
  struct aw_mtl* loader_mtl;
  struct vxl_system* vxl;
  struct polygon* polygon; /* Used to triangulate polygonal faces */
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE void
clear_vxaw(struct vxaw* vxaw)
{
  ASSERT(vxaw);
  POLYGON(clear(vxaw->polygon));
  AW(obj_clear(vxaw->loader_obj));
  AW(mtl_clear(vxaw->loader_mtl));
}

static INLINE float*
xyz_to_rgb(float rgb[3], const float xyz[3])
{
  const float M[9] = {
    2.3706743f,-0.5138850f, 0.0052982f,
   -0.9000405f, 1.4253036f,-0.0146949f,
   -0.4706338f, 0.0885814f, 1.0093968f
  };
  return f33_mulf3(rgb, M, xyz);
}

static INLINE void
rgb_f32_to_rgb_u8(unsigned char rgb_u8[3], const float rgb_f32[3])
{
  int i;
  ASSERT(rgb_f32 && rgb_u8);
  FOR_EACH(i, 0, 3) {
    rgb_u8[i] = (unsigned char)(CLAMP(rgb_f32[i], 0.f, 1.f) * 255.f);
  }
}

static void
triangle_soup_init(struct mem_allocator* allocator, struct triangle_soup* soup)
{
  ASSERT(soup);
  darray_mtl_init(allocator, &soup->mtls);
  htable_mtl_init(allocator, &soup->str2mtl);
  darray_vertex_init(allocator, &soup->vertices);
  darray_size_t_init(allocator, &soup->mtlids);
}

static void
triangle_soup_release(struct triangle_soup* soup)
{
  ASSERT(soup);
  darray_mtl_release(&soup->mtls);
  htable_mtl_release(&soup->str2mtl);
  darray_vertex_release(&soup->vertices);
  darray_size_t_release(&soup->mtlids);
}

static FINLINE size_t
triangle_soup_get_triangles_count(const struct triangle_soup* soup)
{
  ASSERT(soup);
  return darray_size_t_size_get(&soup->mtlids);
}

static void
triangle_indices_get(const uint64_t itri, uint64_t ids[3], void* ctx)
{
  ASSERT(ids);
  (void)ids, (void)ctx;
  ids[0] = itri*3 + 0;
  ids[1] = itri*3 + 1;
  ids[2] = itri*3 + 2;
}

static void
vertex_position_get(const uint64_t ivertex, float pos[3], void* ctx)
{
  const struct triangle_soup* soup = ctx;
  const struct aw_obj_vertex* vertex;
  ASSERT(ctx && pos && ivertex < darray_vertex_size_get(&soup->vertices));
  vertex = darray_vertex_cdata_get(&soup->vertices) + ivertex;
  f3_set(pos, vertex->position);
}

static void
triangle_normal_get
  (const uint64_t itri,
   const float uvw[3],
   float normal[3],
   void* ctx)
{
  uint64_t ids[3];
  const struct triangle_soup* soup = ctx;
  float v0[3], v1[3], v2[3], e0[3], e1[3];
  ASSERT(ctx && uvw && normal && itri<triangle_soup_get_triangles_count(soup));
  (void)soup, (void)uvw;
  triangle_indices_get(itri, ids, ctx);
  vertex_position_get(ids[0], v0, ctx);
  vertex_position_get(ids[1], v1, ctx);
  vertex_position_get(ids[2], v2, ctx);
  f3_sub(e0, v1, v0);
  f3_sub(e1, v2, v0);
  f3_cross(normal, e0, e1);
  f3_normalize(normal, normal);
}

static void
triangle_color_get
  (const uint64_t itri,
   const float uvw[3],
   unsigned char col[3],
   void* ctx)
{
  const struct triangle_soup* soup = ctx;
  size_t imtl;
  ASSERT(ctx && uvw && col && itri < triangle_soup_get_triangles_count(soup));
  (void)uvw; /* Avoid warning in debug */

  imtl = darray_size_t_cdata_get(&soup->mtlids)[itri];
  if(imtl == SIZE_MAX) {
    VXL_MESH_GET_COLOR_DEFAULT(itri, uvw, col, ctx);
  } else {
    const struct aw_material* mtl = darray_mtl_cdata_get(&soup->mtls) + imtl;
    float tmp[3];
    switch(mtl->diffuse.color_space) {
      case AW_COLOR_RGB:
        rgb_f32_to_rgb_u8(col, mtl->diffuse.value);
        break;
      case AW_COLOR_XYZ:
        rgb_f32_to_rgb_u8(col, xyz_to_rgb(tmp, mtl->diffuse.value));
        break;
      default: FATAL("Unreachable code!\n"); break;
    }
  }
}

static res_T
shape_register
  (struct vxaw* vxaw,
   struct triangle_soup* soup,
   const struct aw_obj_named_group* obj_mtl)
{
  size_t* pimtl;
  size_t imtl;
  size_t iface;
  res_T res = RES_OK;
  ASSERT(vxaw && soup && obj_mtl);

  pimtl = htable_mtl_find(&soup->str2mtl, &obj_mtl->name);
  imtl = pimtl ? *pimtl : SIZE_MAX;

  /* Register the shape data from the loaded obj into the mesh */
  FOR_EACH(iface, obj_mtl->face_id, obj_mtl->face_id + obj_mtl->faces_count) {
    struct aw_obj_face face;
    struct aw_obj_vertex vertex;
    size_t i;
    const uint32_t* ids;
    uint32_t nids;

    AW(obj_face_get(vxaw->loader_obj, iface, &face));

    /* Triangulate the face */
    POLYGON(clear(vxaw->polygon));
    FOR_EACH(i, face.vertex_id, face.vertex_id + face.vertices_count) {
      struct aw_obj_vertex vert;
      AW(obj_vertex_get(vxaw->loader_obj, i, &vert));
      res = polygon_vertex_add(vxaw->polygon, vert.position);
      if(res != RES_OK) goto error;
    }
    res = polygon_triangulate(vxaw->polygon, &ids, &nids);
    if(res != RES_OK) goto error;

    /* Register the triangle vertices */
    FOR_EACH(i, 0, nids) {
      const size_t ivertex = ids[i] + face.vertex_id;
      AW(obj_vertex_get(vxaw->loader_obj, ivertex, &vertex));
      res = darray_vertex_push_back(&soup->vertices, &vertex);
      if(res != RES_OK) goto error;
    }

    /* Register the per triangle material */
    FOR_EACH(i, 0, nids/3) {
      res = darray_size_t_push_back(&soup->mtlids, &imtl);
      if(res != RES_OK) goto error;
    }
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
load_materials
  (struct vxaw* vxaw, struct triangle_soup* soup, const char* filename)
{
  struct darray_char scratch; /* Used to store mtllib name */
  size_t imtllib;
  struct aw_obj_desc obj_desc;
  res_T res = RES_OK;
  ASSERT(vxaw && soup && filename);

  darray_char_init(vxaw->allocator, &scratch);
  AW(obj_desc_get(vxaw->loader_obj, &obj_desc));

  FOR_EACH(imtllib, 0, obj_desc.mtllibs_count) {
    struct aw_material* mtls;
    const char* filedir;
    const char* mtllib;
    size_t imtls;
    size_t imtl, nmtls;
    size_t mtllib_len;
    size_t filedir_len;

    /* Retrieve the material library */
    AW(obj_mtllib_get(vxaw->loader_obj, imtllib, &mtllib));

    /* Setup the `scratch' with the directory of the filename */
    res = darray_char_resize(&scratch, strlen(filename) + 1/*'\0'*/);
    if(res != RES_OK) goto error;
    strcpy(darray_char_data_get(&scratch), filename);
    filedir = dirname(darray_char_data_get(&scratch));
    filedir_len = strlen(filedir);
    res = darray_char_resize(&scratch, filedir_len + 1/*+1 <=> '/'*/);
    if(res != RES_OK) goto error;
    memmove(darray_char_data_get(&scratch), filedir, filedir_len);
    darray_char_data_get(&scratch)[filedir_len] = '/';

    /* Append the mtllib name to the directory stored into the `scratch' */
    mtllib_len = strlen(mtllib);
    res = darray_char_resize(&scratch,  mtllib_len + 1/*+1<=> '\0'*/);
    if(res != RES_OK) goto error;
    strcpy(darray_char_data_get(&scratch) + filedir_len + 1, mtllib);

    /* Check that the material lib file exists */
    if(access(darray_char_cdata_get(&scratch), F_OK)) continue;

    /* Load the material lib */
    res = aw_mtl_load(vxaw->loader_mtl, darray_char_cdata_get(&scratch));
    if(res != RES_OK) goto error;

    AW(mtl_materials_count_get(vxaw->loader_mtl, &nmtls));

    /* Allocate materials */
    imtls = darray_mtl_size_get(&soup->mtls);
    res = darray_mtl_resize(&soup->mtls, imtls + nmtls);
    if(res != RES_OK) goto error;
    mtls = darray_mtl_data_get(&soup->mtls) + imtls;

    /* Register the loaded materials */
    FOR_EACH(imtl, 0, nmtls) {
      const size_t id = imtl + imtls;
      AW(mtl_material_get(vxaw->loader_mtl, imtl, mtls + imtl));

      res = htable_mtl_set(&soup->str2mtl, &mtls[imtl].name, &id);
      if(res != RES_OK) goto error;
    }
  }

exit:
  darray_char_release(&scratch);
  return res;
error:
  goto exit;
}

static res_T
scene_create
  (struct vxaw* vxaw,
   const size_t definition,
   const enum vxl_mem_location mem_location,
   const char* filename,
   struct vxaw_report* report, /* May be NULL */
   struct vxl_scene** out_scn)
{
  struct aw_obj_named_group grp;
  struct aw_obj_desc obj_desc;
  struct vxl_scene* scn = NULL;
  struct vxl_mesh_desc desc = VXL_MESH_DESC_NULL;
  struct triangle_soup soup;
  size_t imtl;
  res_T res = RES_OK;
  ASSERT(vxaw && definition && filename && out_scn);

  AW(obj_named_group_init(vxaw->allocator, &grp));
  AW(obj_desc_get(vxaw->loader_obj, &obj_desc));
  triangle_soup_init(vxaw->allocator, &soup);

  /* Create a new scene */
  res = vxl_scene_create(vxaw->vxl, &scn);
  if(res != RES_OK) goto error;

  if(!obj_desc.usemtls_count) {
    /* Create a fictive group that contains all the triangles and register it
     * into the triangle soup */
    str_clear(&grp.name);
    grp.face_id = 0;
    grp.faces_count = obj_desc.faces_count;
    res = shape_register(vxaw, &soup, &grp);
    if(res != RES_OK) goto error;
  } else {
    /* Preload the materials */
    res = load_materials(vxaw, &soup, filename);
    if(res != RES_OK) goto error;
    /* Register each obj shape into the triangle soup */
    FOR_EACH(imtl, 0, obj_desc.usemtls_count) {
      AW(obj_mtl_get(vxaw->loader_obj, imtl, &grp));
      res = shape_register(vxaw, &soup, &grp);
      if(res != RES_OK) goto error;
    }
  }

  /* Purge the AW loaders to free up some memory */
  AW(obj_purge(vxaw->loader_obj));
  AW(mtl_purge(vxaw->loader_mtl));

  /* Setup the VXL scene */
  desc.get_ids = triangle_indices_get;
  desc.get_pos = vertex_position_get;
  desc.get_normal = triangle_normal_get;
  desc.get_color = triangle_color_get;
  desc.data = &soup;
  desc.ntris = triangle_soup_get_triangles_count(&soup);

  if(report) {
    res = vxl_scene_setup(scn, &desc, 1, definition, mem_location,
      &report->voxelization, &report->build);
  } else {
    res = vxl_scene_setup(scn, &desc, 1, definition, mem_location,
      NULL, NULL);
  }
  if(res != RES_OK) goto error;

exit:
  *out_scn = scn;
  triangle_soup_release(&soup);
  AW(obj_named_group_release(&grp));
  return res;
error:
  if(scn) {
    VXL(scene_ref_put(scn));
    scn = NULL;
  }
  goto exit;
}

static void
release_vxaw(ref_T* ref)
{
  struct vxaw* vxaw = CONTAINER_OF(ref, struct vxaw, ref);
  ASSERT(ref);

  if(vxaw->loader_obj) AW(obj_ref_put(vxaw->loader_obj));
  if(vxaw->loader_mtl) AW(mtl_ref_put(vxaw->loader_mtl));
  if(vxaw->vxl) VXL(system_ref_put(vxaw->vxl));
  if(vxaw->polygon) POLYGON(ref_put(vxaw->polygon));
  MEM_RM(vxaw->allocator, vxaw);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
vxaw_create
  (struct logger* logger,
   struct mem_allocator* allocator,
   struct aw_obj* obj,
   struct aw_mtl* mtl,
   struct vxl_system* vxl,
   struct vxaw** out_vxaw)
{
  struct vxaw* vxaw = NULL;
  struct mem_allocator* mem_allocator;
  res_T res;

  if(!vxl || !out_vxaw) {
    res = RES_BAD_ARG;
    goto error;
  }
  mem_allocator = allocator ? allocator : &mem_default_allocator;
  vxaw = MEM_CALLOC(mem_allocator, 1, sizeof(struct vxaw));
  if(!vxaw) {
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&vxaw->ref);
  vxaw->allocator = mem_allocator;
  vxaw->logger = logger ? logger : LOGGER_DEFAULT;
  VXL(system_ref_get(vxl));
  vxaw->vxl = vxl;

  res = polygon_create(vxaw->allocator, &vxaw->polygon);
  if(res != RES_OK) goto error;

  if(obj) {
    AW(obj_ref_get(obj));
    vxaw->loader_obj = obj;
  } else {
    res = aw_obj_create(logger, allocator, 0, &vxaw->loader_obj);
    if(res != RES_OK) goto error;
  }
  if(mtl) {
    AW(mtl_ref_get(mtl));
    vxaw->loader_mtl = mtl;
  } else {
    res = aw_mtl_create(logger, allocator, 0, &vxaw->loader_mtl);
    if(res != RES_OK) goto error;
  }

exit:
  if(out_vxaw)
    *out_vxaw = vxaw;
  return res;
error:
  if(vxaw) {
    VXAW(ref_put(vxaw));
    vxaw = NULL;
  }
  goto exit;
}

res_T
vxaw_ref_get(struct vxaw* vxaw)
{
  if(!vxaw) return RES_BAD_ARG;
  ref_get(&vxaw->ref);
  return RES_OK;
}

res_T
vxaw_ref_put(struct vxaw* vxaw)
{
  if(!vxaw) return RES_BAD_ARG;
  ref_put(&vxaw->ref, release_vxaw);
  return RES_OK;
}

res_T
vxaw_get_loaders(struct vxaw* vxaw, struct aw_obj** obj, struct aw_mtl** mtl)
{
  if(!vxaw) return RES_BAD_ARG;
  if(obj) *obj = vxaw->loader_obj;
  if(mtl) *mtl = vxaw->loader_mtl;
  return RES_OK;
}

res_T
vxaw_load
  (struct vxaw* vxaw,
   const size_t definition,
   const enum vxl_mem_location mem_location,
   const char* filename,
   struct vxaw_report* report,
   struct vxl_scene** out_scn)
{
  struct vxl_scene* scn = NULL;
  struct time t0, t1;
  res_T res = RES_OK;

  if(!vxaw || !definition || !filename || !out_scn) {
    res = RES_BAD_ARG;
    goto error;
  }

  if(report) time_current(&t0);
  res = aw_obj_load(vxaw->loader_obj, filename);
  if(res != RES_OK) goto error;
  if(report) {
    time_current(&t1);
    time_sub(&report->load, &t1, &t0);
  }

  res = scene_create(vxaw, definition, mem_location, filename, report, &scn);
  if(res != RES_OK) goto error;

exit:
  if(out_scn) *out_scn = scn;
  return res;
error:
  if(vxaw) clear_vxaw(vxaw);
  if(scn) {
    VXL(scene_ref_put(scn));
    scn = NULL;
  }
  goto exit;
}

res_T
vxaw_load_stream
  (struct vxaw* vxaw,
   const size_t definition,
   const enum vxl_mem_location mem_location,
   FILE* stream,
   struct vxaw_report* report,
   struct vxl_scene** out_scn)
{
  struct vxl_scene* scn = NULL;
  struct time t0, t1;
  res_T res = RES_OK;

  if(!vxaw || !definition || !stream || !out_scn) {
    res = RES_BAD_ARG;
    goto error;
  }

  if(report) time_current(&t0);
  res = aw_obj_load_stream(vxaw->loader_obj, stream);
  if(res != RES_OK) goto error;
  if(report) {
    time_current(&t1);
    time_sub(&report->load, &t1, &t0);
  }

  res = scene_create(vxaw, definition, mem_location, "./", report, &scn);
  if(res != RES_OK) goto error;

exit:
  if(out_scn) *out_scn = scn;
  return res;
error:
  if(vxaw) clear_vxaw(vxaw);
  if(scn) {
    VXL(scene_ref_put(scn));
    scn = NULL;
  }
  goto exit;
}

