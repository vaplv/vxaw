# VoXelizer of Alias Wavefront objects

This code base voxelizes objects saved with respect to the Alias Wavefront
obj/mtl fileformats. It relies on the AW library to load Alias Wavefront
objects, on the Polygon library to triangulate their polygonal faces and on the
VxL engine to voxelize triangular meshes. The result is a VxL scene.

Note that currently, there is no specific voxel shading based onto the Alias
Wavefront material; the voxel color is simply the normal of the underlying
triangle sample.

## How to build

The library uses [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) package to build. It also
depends on the [AW](https://gitlab.com/vaplv/loader_aw/), the
[Polygon](https://gitlab.com/vaplv/polygon/), the
[RSys](https://gitlab.com/vaplv/rsys/) and the
[VxL](https://gitlab.com/vaplv/vxl/) libraries. First install the RCMake
package as well as all the library dependencies. Then generate the project from
the cmake/CMakeLists.txt file by appending to the `CMAKE_PREFIX_PATH` variable
the install directory of the RCMake package and the ones of the AW, Polygon,
RSys and VxL libraries.

## Release notes

### Version 0.3

- Bump the version of the dependencies and adapt the code to their updates.

## License

VxAW is Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr). It is a free
software released under the [OSI](https://opensource.org)-approved GPL v3+
license. You are welcome to redistribute it under certain conditions; refer to
the COPYING file for details.

